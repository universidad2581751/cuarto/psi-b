# To compile.

javac -cp .:jade.jar *.java
javac -cp .:jade.jar agents/*.java


# Random agent:

It chooses a random play between the two independently of the previous results

**Execute**

java -cp .:jade.jar jade.Boot -notmp -gui -agents "MainAgent:MainAgent;RAg1:agents.RandomAgent;RAg2:agents.RandomAgent;RAg3:agents.RandomAgent;RAg4:agents.RandomAgent;RAg5:agents.RandomAgent;RAg6:agents.RandomAgent;RAg7:agents.RandomAgent;RAg8:agents.RandomAgent;RAg9:agents.RandomAgent;RAg10:agents.RandomAgent;RAg11:agents.RandomAgent;RAg12:agents.RandomAgent;RAg13:agents.RandomAgent;RAg14:agents.RandomAgent;RAg15:agents.RandomAgent;RAg16:agents.RandomAgent;RAg17:agents.RandomAgent;RAg18:agents.RandomAgent;RAg19:agents.RandomAgent;RAg20:agents.RandomAgent;RAg21:agents.RandomAgent;RAg22:agents.RandomAgent;RAg23:agents.RandomAgent;RAg24:agents.RandomAgent;RAg25:agents.RandomAgent;RAg26:agents.RandomAgent;RAg27:agents.RandomAgent;RAg28:agents.RandomAgent;RAg29:agents.RandomAgent;RAg30:agents.RandomAgent;RAg31:agents.RandomAgent;RAg32:agents.RandomAgent;RAg33:agents.RandomAgent;RAg34:agents.RandomAgent;RAg35:agents.RandomAgent"


# Learning Automata

It begins choosing a random result (50% probability each) and it keep rewarding the best plays, so in the end it tends to choose a play more than another

**Execute**

java -cp .:jade.jar jade.Boot -notmp -gui -agents "MainAgent:MainAgent;RAg1:agents.RLAgent;RAg2:agents.RLAgent;RAg3:agents.RLAgent;RAg4:agents.RLAgent;RAg5:agents.RLAgent;RAg6:agents.RLAgent;RAg7:agents.RLAgent;RAg8:agents.RLAgent;RAg9:agents.RLAgent;RAg10:agents.RLAgent;RAg11:agents.RLAgent;RAg12:agents.RLAgent;RAg13:agents.RLAgent;RAg14:agents.RLAgent;RAg15:agents.RLAgent;RAg16:agents.RLAgent;RAg17:agents.RLAgent;RAg18:agents.RLAgent;RAg19:agents.RLAgent;RAg20:agents.RLAgent;RAg21:agents.RLAgent;RAg22:agents.RLAgent;RAg23:agents.RLAgent;RAg24:agents.RLAgent;RAg25:agents.RLAgent;RAg26:agents.RLAgent;RAg27:agents.RLAgent;RAg28:agents.RLAgent;RAg29:agents.RLAgent;RAg30:agents.RLAgent;RAg31:agents.RLAgent;RAg32:agents.RLAgent;RAg33:agents.RLAgent;RAg34:agents.RLAgent;RAg35:agents.RLAgent"


# Neural network

Tries to select the best play using the neuron training and basing its decission in Learning Automata

**Execute**

java -cp .:jade.jar jade.Boot -notmp -gui -agents "MainAgent:MainAgent;RAg1:agents.NNAgent;RAg2:agents.NNAgent;RAg3:agents.NNAgent;RAg4:agents.NNAgent;RAg5:agents.NNAgent;RAg6:agents.NNAgent;RAg7:agents.NNAgent;RAg8:agents.NNAgent;RAg9:agents.NNAgent;RAg10:agents.NNAgent;RAg11:agents.NNAgent;RAg12:agents.NNAgent;RAg13:agents.NNAgent;RAg14:agents.NNAgent;RAg15:agents.NNAgent;RAg16:agents.NNAgent;RAg17:agents.NNAgent;RAg18:agents.NNAgent;RAg19:agents.NNAgent;RAg20:agents.NNAgent;RAg21:agents.NNAgent;RAg22:agents.NNAgent;RAg23:agents.NNAgent;RAg24:agents.NNAgent;RAg25:agents.NNAgent;RAg26:agents.NNAgent;RAg27:agents.NNAgent;RAg28:agents.NNAgent;RAg29:agents.NNAgent;RAg30:agents.NNAgent;RAg31:agents.NNAgent;RAg32:agents.NNAgent;RAg33:agents.NNAgent;RAg34:agents.NNAgent;RAg35:agents.NNAgent"
