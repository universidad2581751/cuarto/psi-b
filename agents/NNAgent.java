package agents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

public class NNAgent extends Agent {

    private State state;
    private AID mainAgent;
    private ACLMessage msg;

    private NeuralNetwork neuralNetwork;


    protected void setup () {

        System.out.println ("Setting up agent " + getAID ().getName ());
        
        state = State.s0NoConfig;
        
        DFAgentDescription dfd = new DFAgentDescription ();
        dfd.setName (getAID ());
        ServiceDescription sd = new ServiceDescription ();
        
        sd.setType ("Player");
        sd.setName ("Game");
        dfd.addServices (sd);
        
        try {
            
            DFService.register (this, dfd);
            
        } catch (Exception e) {
            
            e.printStackTrace ();
        }
        
        neuralNetwork = new NeuralNetwork (4, 2);

        addBehaviour (new Play ());
        System.out.println ("Agent " + getAID ().getName () + " is ready");
    }


    protected void takeDown () {

        try {

            DFService.deregister (this);
            System.out.println ("Me mori");

        } catch (Exception e) {
            
            e.printStackTrace ();
        }
    }


    private enum State {

        s0NoConfig,
        s1AwaitingGame,
        s2Round,
        s3AwaitingResults
    }


    private class Play extends CyclicBehaviour {

        @Override
        public void action () {

            msg = blockingReceive ();

            if (msg != null) {

                switch (state) {

                    case s0NoConfig:

                        if (msg.getContent ().startsWith ("Id#") && msg.getPerformative () == ACLMessage.INFORM) {

                            try {

                                validateSetupMessage (msg);
                                neuralNetwork.resetValues ();
                                state = State.s1AwaitingGame;

                            } catch (NumberFormatException e) {

                                System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Bad message (" + msg.getContent () + ")");
                            }

                            break;

                        } else if (msg.getContent ().equals ("Remove")) {

                            takeDown ();
                            break;
                        }
                    
                        System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Unexpected message (" + msg.getContent () + ")");
                        break;
                    
                    
                    case s1AwaitingGame:
                    
                        if (msg.getPerformative () == ACLMessage.INFORM) {

                            if (msg.getContent ().startsWith ("Id#")) {

                                try {

                                    validateSetupMessage (msg);

                                } catch (NumberFormatException e) {

                                    System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Bad message (" + msg.getContent () + ")");
                                }

                                break;
                            
                            } else if (msg.getContent ().startsWith ("NewGame#")) {

                                try {
                                    
                                    validateNewGameMessage (msg);
                                    state = State.s2Round;

                                } catch (NumberFormatException e) {

                                    System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Bad message (" + msg.getContent () + ")");
                                }

                                break;

                            } else if (msg.getContent ().equals ("Remove")) {

                                takeDown ();
                                break;
                            }
                        }

                        System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Unexpected message (" + msg.getContent () + ")");
                        break;


                    case s2Round:

                        if (msg.getPerformative () == ACLMessage.INFORM && msg.getContent ().startsWith ("GameOver#")) {
                        
                            state = State.s1AwaitingGame;

                        } else if (msg.getPerformative () == ACLMessage.REQUEST && msg.getContent ().startsWith ("Action")) {

                            decidePlay ();
                            state = State.s3AwaitingResults;

                        } else {

                            System.out.println (getAID ().getName ().split ("@") [0] + ": " + state.name () + " - Bad message (" + msg.getContent () + ")");
                        }

                        break;


                    case s3AwaitingResults:

                        if (msg.getPerformative () == ACLMessage.INFORM && msg.getContent ().startsWith ("Results#")) {

                            state = State.s2Round;
                        }
                }
            }
        }

        
        private void validateSetupMessage (ACLMessage msg) throws NumberFormatException {
            
            String content = msg.getContent ();
            String [] contentSplit = content.split ("#");
            
            mainAgent = msg.getSender ();
            contentSplit = contentSplit [2].split (",");
        }
        
        
        private void validateNewGameMessage (ACLMessage msg) throws NumberFormatException {
            
            String content = msg.getContent ();
            String [] contentSplit = content.split ("#");
            contentSplit = contentSplit [1].split (",");
        }
        

        public void decidePlay () {

            ACLMessage msg = new ACLMessage (ACLMessage.INFORM);
            msg.addReceiver (mainAgent);
            msg.setContent(neuralNetwork.getMbu (new double []{0.5, 0.5}));
            send(msg);    
        }
    }



    public class NeuralNetwork {

        private static final double MIN_LEARN_RATE = 0.1;

        private int gridSide, inputSide, radio;
        private int [] bmuPos = new int [3];

        private double learnRate = 1.0, decLearnRate = 0.999;
        private double [][][] grid;

        
        public NeuralNetwork (int side, int input) {

            gridSide = side;
            inputSide = input;
            radio = side / 10;

            grid = new double [side][side][input];
        }


        public void resetValues () {

            learnRate = 1.0;
            bmuPos [0] = -1;
            bmuPos [1] = -1;
            
            for (int i = 0; i < gridSide; i++) {
                for (int j = 0; j < gridSide; j++) {

                    grid [j][i][0] = Math.random ();
                    grid [j][i][1] = 1 - grid [j][i][0];
                }
            }
        }


        public String getMbu (double [] dmInput) {

            int gridX = 0, gridY = 0;
            double norm, normMin = Double.MAX_VALUE, decission;

            for (int i = 0; i < gridSide; i++) {
                for (int j = 0; j < gridSide; j++) {

                    norm = 0;

                    for (int k = 0; k < inputSide; k++) {

                        norm += (dmInput [k] - grid [j][i][k]) * (dmInput [k] - grid [j][i][k]);
                    }

                    if (norm < normMin) {

                        gridX = i;
                        gridY = j;
                    }
                }
            }

            if (learnRate >= MIN_LEARN_RATE) {

                int gridXaux = 0, gridYaux = 0;

                for (int i = -radio; i <= radio; i++) {
                    for (int j = -radio; j <= radio; j++) {

                        gridXaux = gridX + j;
                        gridYaux = gridY + i;

                        if (gridXaux < 0) {

                            gridXaux += gridSide;

                        } else if (gridXaux >= gridSide) {

                            gridXaux -= gridSide;
                        }

                        if (gridYaux < 0) {

                            gridYaux += gridSide;

                        } else if (gridYaux >= gridSide) {

                            gridYaux -= gridSide;
                        }

                        for (int k = 0; k < inputSide; k++) {

                            grid [gridXaux][gridYaux][k] += learnRate * (dmInput [k] - grid [gridXaux][gridYaux][k]) / (1 + i * i + j * j);
                        }
                    }
                }

                learnRate *= decLearnRate;
            }

            bmuPos [0] = gridX;
            bmuPos [1] = gridY;

            decission = (Math.random () * 2);

            if (Double.compare (decission, grid [gridX][gridY][0]) <= 0) {

                bmuPos [2] = 0;
                return "Action#D";

            } else {
                
                bmuPos [2] = 1;
                return "Action#H";
            }
        }
    }



    public class PlayerInformation {

        int id, points = 0;

        public PlayerInformation (int id) {

            this.id = id;
        }

        public void addPoints (int added) {

            points += added;
        }
    }
}
